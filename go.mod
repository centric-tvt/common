module gitlab.com/goxp/cloud0

go 1.16

require (
	github.com/caarlos0/env/v6 v6.6.2
	github.com/gin-gonic/gin v1.7.2
	github.com/go-errors/errors v1.4.0
	github.com/go-playground/validator/v10 v10.6.1
	github.com/google/uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	gorm.io/driver/postgres v1.1.0
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.11
)
